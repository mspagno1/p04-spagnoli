//
//  GameScene.m
//  SideScroller
//
//  Created by Vaster on 3/11/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameScene.h"


//Masks of the two coliding objects
static const uint32_t spikeCategory =  0x1 << 0;
static const uint32_t playerCategory =  0x1 << 1;
static const uint32_t groundCategory =  0x1 << 2;
static const uint32_t backgroundCategory =  0x1 << 3;



@implementation GameScene {
    NSTimeInterval _lastUpdateTime;
    SKSpriteNode * background[7];
    SKSpriteNode * ground;
    SKSpriteNode * player;
    SKSpriteNode * spike;
    
    SKAction* jumpLeft;
    SKAction* walkLeft;
    SKAction* jumpRight;
    SKAction* walkRight;
    
    SKAction* spikewalkLeft;
    SKAction* spikewalkRight;
    //SKCameraNode * cam;
    bool spikeDirLeft;
    int spikeXSpeed;
    int spikeYSpeed;

    
    //Keeps track of which background to use
    SKAction *moveBackground;
    int currentScreen;
    bool addAllBack;
    

    
}

- (void)sceneDidLoad {
    // Setup your scene here
    currentScreen = 0;
    addAllBack = false;
    
    // Initialize update time
    _lastUpdateTime = 0;
    
    //cam = [SKCameraNode node];
    //self.camera = cam;
    
    background[0] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount1.png"];
    background[1] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount2.png"];
    background[2] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount3.png"];
    background[3] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount4.png"];
    background[4] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount1.png"];
    background[5] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount2.png"];
    //background[6] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount3.png"];
    //background[7] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount4.png"];
    //background[8] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount1.png"];
    //background[9] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount2.png"];
    //background[10] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount3.png"];
    //background[11] = [SKSpriteNode spriteNodeWithImageNamed: @"Mount4.png"];
    background[6] = [SKSpriteNode spriteNodeWithImageNamed: @"GroundLvl.png"];

    
    //Set up background
    [self makeBackground];
    //moveBackground = [SKAction moveByX: -50 y:0 duration: 0.2];
    //[background[0] runAction: moveBackground];
    //Set up ground
    
    
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    ground.physicsBody.categoryBitMask = groundCategory;
    
    [self addChild: (ground)];
    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -3.0 );
    self.physicsWorld.contactDelegate = self;
    
    //Set up player
    
    
    
    [self makeAnimation];
    [self makePlayer];
    [self makeSpike];
    
    spikeXSpeed = 5;
    spikeYSpeed = 0;


}

-(void) reset{
    for(int i = 0; i <currentScreen; i++){
        [background[i] removeFromParent];
    }
    [ground removeFromParent];
    [player removeFromParent];
    [spike removeFromParent];
    currentScreen = 0;
    
    //Set up background
    [self makeBackground];
    
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:ground.texture.size];;
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    [self addChild: (ground)];
    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -3.0 );
    
    [self makePlayer];
    [self makeSpike];
    
    spikeXSpeed = 5;
    spikeYSpeed = 0;
    
}

-(void) makeBackground{
    background[currentScreen].size = self.frame.size;
    [background[currentScreen] setPosition: CGPointMake(0,0)];
    background[currentScreen].physicsBody.affectedByGravity = FALSE;
    background[currentScreen].physicsBody.categoryBitMask = backgroundCategory;
    [self addChild: (background[currentScreen])];
    
    
    currentScreen++;
    
    if(currentScreen > 6){
        SKPhysicsBody *border = [SKPhysicsBody bodyWithEdgeLoopFromRect: self.frame];
        
        self.physicsBody = border;
        //Were at the end wooh!
       
    }
}
-(void) makeLevel{
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    ground.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (ground)];

    

}

-(void) makePlayer{
    //Set up player
    SKTexture* CelesGround = [SKTexture textureWithImageNamed:@"Celes.gif"];
    player = [SKSpriteNode spriteNodeWithTexture: CelesGround];
    
    
    [player setPosition: CGPointMake(-300,-517)];
    player.size = CGSizeMake(80, 100);
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    //player.physicsBody = [SKPhysicsBody bodyWithTexture:player.texture size:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    player.physicsBody.categoryBitMask = playerCategory;
    player.physicsBody.contactTestBitMask = spikeCategory;
    //player.physicsBody.collisionBitMask = 0;
    player.physicsBody.usesPreciseCollisionDetection = YES;

    
    [self addChild: (player)];
}

-(void) makeSpike{
    //Set up player
    
    SKTexture* SpikeStart = [SKTexture textureWithImageNamed:@"SpikeLeft2.tiff"];
    spike = [SKSpriteNode spriteNodeWithTexture: SpikeStart];
    
    
    
    [spike setPosition: CGPointMake(200,-517)];
    spike.size = CGSizeMake(70, 80);
    
    spike.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:spike.texture.size];
    spike.physicsBody.dynamic = YES;
    spike.physicsBody.allowsRotation = false;
    
    spike.physicsBody.categoryBitMask = spikeCategory;
    spike.physicsBody.contactTestBitMask = playerCategory;
    //spike.physicsBody.collisionBitMask = 0;
    spike.physicsBody.usesPreciseCollisionDetection = YES;

    
    spikeDirLeft = true;
    [spike runAction:spikewalkLeft];
    [self addChild: (spike)];
}

-(void) makeAnimation{
    
    //Walk action Left
    SKTexture* CelesWalkLeft1 = [SKTexture textureWithImageNamed:@"walkLeft1.tiff"];
    CelesWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft2 = [SKTexture textureWithImageNamed:@"walkLeft2.tiff"];
    CelesWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft3 = [SKTexture textureWithImageNamed:@"walkLeft3.tiff"];
    CelesWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft4 = [SKTexture textureWithImageNamed:@"walkLeft4.tiff"];
    CelesWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    walkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkLeft1,CelesWalkLeft2,CelesWalkLeft3,CelesWalkLeft4] timePerFrame:0.2]];
    
    
    //Walk action Right
    SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"walkRight1.tiff"];
    CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"walkRight2.tiff"];
    CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"walkRight3.tiff"];
    CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"walkRight4.tiff"];
    CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
    
    //Jump action Left
    SKTexture* CelesJumpLeft1 = [SKTexture textureWithImageNamed:@"jumpLeft1.png"];
    CelesJumpLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesJumpLeft2 = [SKTexture textureWithImageNamed:@"jumpLeft2.tiff"];
    CelesJumpLeft2.filteringMode = SKTextureFilteringNearest;
    
    jumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpLeft1, CelesJumpLeft2] timePerFrame:0.2]];
    
    //Jump action Right
    SKTexture* CelesJumpRight1 = [SKTexture textureWithImageNamed:@"jumpRight1.tiff"];
    CelesJumpRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesJumpRight2 = [SKTexture textureWithImageNamed:@"jumpRight2.tiff"];
    CelesJumpRight2.filteringMode = SKTextureFilteringNearest;
    
    jumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpRight1, CelesJumpRight2] timePerFrame:0.2]];
    
    //Spike
    
    //Walk action Left
    SKTexture* SpikeWalkLeft1 = [SKTexture textureWithImageNamed:@"SpikeLeft1.tiff"];
    SpikeWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* SpikeWalkLeft2 = [SKTexture textureWithImageNamed:@"SpikeLeft2.tiff"];
    SpikeWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    spikewalkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[SpikeWalkLeft1,SpikeWalkLeft2] timePerFrame:0.2]];
    
    
    //Walk action Right
    SKTexture* SpikeWalkRight1 = [SKTexture textureWithImageNamed:@"SpikeRight1.tiff"];
    SpikeWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* SpikeWalkRight2 = [SKTexture textureWithImageNamed:@"SpikeRight2.tiff"];
    SpikeWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    
    spikewalkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[SpikeWalkRight1,SpikeWalkRight2,] timePerFrame:0.2]];

}


-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    if(location.y <= player.position.y){
        //if(player.physicsBody.velocity.dx == 0){
            if(location.x > player.position.x && location.y){
                //[background[0] runAction: moveBackground];
                [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
                [player runAction:walkRight];
            }
            else{
                [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                [player runAction:walkLeft];
            }
        //}
    }
    else{
        if(player.physicsBody.velocity.dy == 0){
            if(location.x > player.position.x && location.y){
                [player.physicsBody applyImpulse:CGVectorMake(5, 25)];
                [player runAction:jumpRight];
            }
            else{
                [player.physicsBody applyImpulse:CGVectorMake(-5, 25)];
                [player runAction:jumpLeft];
            }
        }
        
    }
    
}

//Code from segment from http://blog.rickermortes.com/post/79659100868/proper-collision-detection-with-sprite-kit

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask == spikeCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        printf("Collison");
        //Not sure why this doesnt work[player setPosition: CGPointMake(-300,-517)];
        [player removeFromParent];
        [self makePlayer];
        
    }
}


-(void)update:(CFTimeInterval)currentTime {
    //cam.position = player.position;
    //check for each enenmy if the player is on the same cordinate as the enemy if so game over function
    //will probs save enemy in a array
    if(spike.physicsBody.velocity.dx == 0 && spikeDirLeft == true){
        if(spike.physicsBody.velocity.dy == 0 && spikeDirLeft == true){
            [spike.physicsBody applyImpulse:CGVectorMake(-spikeXSpeed, spikeYSpeed)];
        }
    }
    else if(spike.physicsBody.velocity.dx == 0 && spikeDirLeft == false){
        if(spike.physicsBody.velocity.dy == 0 && spikeDirLeft == false){
            [spike.physicsBody applyImpulse:CGVectorMake(spikeXSpeed, spikeYSpeed)];
        }
    }
    if(spike.position.x < -340){
        [spike runAction:spikewalkRight];
        spikeDirLeft = false;
    }
    else if(spike.position.x > 340){
        [spike runAction:spikewalkLeft];
        spikeDirLeft = true;
    }
    
    if(player.position.x > 370){
        //If player reachs end screen generate new level area
        [background[currentScreen-1] removeFromParent];
        [self makeBackground];
        
        //Generate level here
        //ground.physicsBody = nil;
        
        [ground removeFromParent];
        [self makeLevel];
      
        [player removeFromParent];
        [self makePlayer];
        [spike removeFromParent];
        if(currentScreen < 7){
            
        
            [self makeSpike];
        
            spikeXSpeed = arc4random_uniform(15);

            spikeYSpeed = arc4random_uniform(20);
        }
        else{
            //You win
        }

    }

}

@end
