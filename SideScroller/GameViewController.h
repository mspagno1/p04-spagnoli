//
//  GameViewController.h
//  SideScroller
//
//  Created by Vaster on 3/11/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface GameViewController : UIViewController

@end
