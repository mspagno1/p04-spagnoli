Matthew Spagnoli

Avoid spike and make it to the base of the tower to win!

Controls:
Click to the left or right of the character on the same y level or lower to move left or right.
Click to the left or right above the characters current y level to jump in that direction. Can only jump once and it resets once you touched a structure or the ground.